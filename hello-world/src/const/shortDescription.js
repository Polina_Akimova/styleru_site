export const description = {
    'web-frontend': 'HTML, CSS, JS, JQuery, VUE',
    'web-backend': 'Python, Django Framework',
    'ios': 'URLSessions, Grand Central Dispatch, UIKit, iOS SDK, Core Data, Realm и др.',
    'android': 'App Architecture, Design, Multithreading, Dependency Injection и др.',
    'web-дизайн': 'Photoshop, Figma, анимация и освоение ReadyMag',
    'graph-дизайн': 'Разработка цифрового и физического дизайна, изучение программ Adobe',
    'контент-маркетинг': 'Изучение копирайтинга, основ дизайна, SMM и Target, создание лендингов',
    'маркетинговая-аналитика': 'Настройка рекламных объявлений, работа с веб- и сквозной аналитикой, изучение CRM и A/B тестирования',
    'project-management': 'Планирование и запуск проектов, разработка концепции, оценка ресурсов, сбор и контроль команды, анализ результатов проекта',
}