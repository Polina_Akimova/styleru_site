import API from 'Api'

const state = {
    projectsList: [],
    project: {}
}

const getters = {
    getProjectsList: state => {
        return state.projectsList
    },
    getProjectInfo: state => {
        return state.project
    }
}

const mutations = {
    setAllProjects(state, payload){
        state.projectsList = payload;
    },
    setProjectInfo(state, payload){
        state.project = payload;
    }
}

const actions = {
    getAllProjects({commit}){
        API
            .post('website/get_projects')
            .then(response => {
                commit('setAllProjects', response.data.info)
            })
            .catch(function (error) {
                console.log(error)
            })
    },
    getProject({commit}, id){
        const data = new FormData();
        data.append('id_project', id);
        API
            .post('website/get_project_info', data)
            .then(response => {
                commit('setProjectInfo', response.data.info)
            })
            .catch(function (error) {
                console.log(error)
            })
    }
}



export default {
    state,
    getters,
    mutations,
    actions
}
