import Vue from 'vue'
import VueRouter from 'vue-router'
// import CoursesPage from '../views/CoursesPage'
// import WebFrontend from "../views/courses/WebFrontend.vue";
import notFound from "@/views/NotFound";
import newMain from "@/views/newMain";
import CoursePage from "@/views/courses/CoursePage";
import Contacts from "@/views/Contacts";
import Courses from "@/views/Courses";
// import MainPageMobile from '../views/MainPageMobile'

Vue.use(VueRouter)

const routes = [
    {
        path: '*',
        component: notFound,
        meta: {
            none: true
        }
    },
    {
        path: '/',
        component: newMain,
    },
    {
        path: '/courses',
        component: Courses,
        // children: [
        //     {
        //         path: '/WebFrontend',
        //         component: WebFrontend
        //     }
        // ]
    },
    {
        path: '/courses/:id',
        component: CoursePage
    },
    {
        path: '/projects',
        component: () => import('../views/projects/AllProjects')
    },
    {
        path: '/project/:project_id',
        component: () => import('../views/projects/ProjectFullInfo')
    },
    {
        path: '/about',
        component: () => import('../views/aboutUs/fullAboutUs')
    },
    {
        path: '/contacts',
        component: Contacts
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
  })

  export default router