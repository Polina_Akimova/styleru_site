import Vue from 'vue';
import App from './App.vue';
import {BootstrapVue, BootstrapVueIcons} from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css'
import '@/css/main.css'
import vuetify from '@/plugins/vuetify'
import 'vuetify/dist/vuetify.min.css';
import router from './router'
import store from './store'
import Vuetify from "vuetify";
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.use(VueMaterial)


Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(Vuetify);

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App),
}).$mount('#app')

console.log(router)
