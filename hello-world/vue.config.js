const path = require('path');


module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  configureWebpack: {
    resolve: {
      alias: {
        Api: path.resolve(__dirname, 'src/api/'),
      },
    }
  }
}